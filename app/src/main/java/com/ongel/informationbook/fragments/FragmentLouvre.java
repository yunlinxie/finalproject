package com.ongel.informationbook.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;


public class FragmentLouvre extends Fragment {

    public static FragmentLouvre newInstance() {
        return new FragmentLouvre();
    }

    private ImageView imageViewLouvre;
    private TextView textViewLouvre;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_louvre,container,false);

        imageViewLouvre = view.findViewById(R.id.imageViewLouvre);
        textViewLouvre = view.findViewById(R.id.textViewLouvre);

        return view;
    }

}
