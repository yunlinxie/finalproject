package com.ongel.informationbook.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;

public class FragmentChichenItza extends Fragment {

    public static FragmentChichenItza newInstance()
    {
        return new FragmentChichenItza();
    }

    private ImageView imageViewChichenItza;
    private TextView textViewChichenItza;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_chichen_itza,container,false);

        imageViewChichenItza = view.findViewById(R.id.imageViewChichenItza);
        textViewChichenItza = view.findViewById(R.id.textViewChichenItza);

        return view;
    }
}