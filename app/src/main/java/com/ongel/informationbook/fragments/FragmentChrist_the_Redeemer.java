package com.ongel.informationbook.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;

public class FragmentChrist_the_Redeemer extends Fragment {

    public static FragmentChrist_the_Redeemer newInstance()
    {
        return new FragmentChrist_the_Redeemer();
    }

    private ImageView imageViewChrist;
    private TextView textViewChrist;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_christ_the_redeemer,container,false);

        imageViewChrist = view.findViewById(R.id.imageViewChrist);
        textViewChrist = view.findViewById(R.id.textViewChrist);

        return view;
    }
}
