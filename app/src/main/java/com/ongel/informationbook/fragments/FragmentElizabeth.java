package com.ongel.informationbook.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;

public class FragmentElizabeth extends Fragment {

    public static FragmentElizabeth newInstance()
    {
        return new FragmentElizabeth();
    }

    private ImageView imageViewElizabeth;
    private TextView textViewElizabeth;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_elizabeth,container,false);

        imageViewElizabeth = view.findViewById(R.id.imageViewElizabeth);
        textViewElizabeth = view.findViewById(R.id.textViewElizabeth);

        return view;
    }
}
