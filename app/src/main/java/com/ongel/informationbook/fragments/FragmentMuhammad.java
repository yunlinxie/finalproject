package com.ongel.informationbook.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;

public class FragmentMuhammad extends Fragment {

    public static FragmentMuhammad newInstance()
    {
        return new FragmentMuhammad();
    }

    private ImageView imageViewMuhammad;
    private TextView textViewMuhammad;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_muhammad,container,false);

        imageViewMuhammad = view.findViewById(R.id.imageViewMuhammad);
        textViewMuhammad = view.findViewById(R.id.textViewMuhammad);

        return view;
    }
}
