package com.ongel.informationbook.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;

public class FragmentPetra extends Fragment {

    public static FragmentPetra newInstance()
    {
        return new FragmentPetra();
    }

    private ImageView imageViewPetra;
    private TextView textViewPetra;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_petra,container,false);

        imageViewPetra = view.findViewById(R.id.imageViewPetra);
        textViewPetra = view.findViewById(R.id.textViewPetra);

        return view;
    }
}
