package com.ongel.informationbook.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;

public class FragmentObama extends Fragment {

    public static FragmentObama newInstance()
    {
        return new FragmentObama();
    }

    private ImageView imageViewObama;
    private TextView textViewObama;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_obama,container,false);

        imageViewObama = view.findViewById(R.id.imageViewObama);
        textViewObama = view.findViewById(R.id.textViewObama);

        return view;
    }
}
