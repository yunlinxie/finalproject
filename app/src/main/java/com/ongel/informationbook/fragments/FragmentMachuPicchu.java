package com.ongel.informationbook.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;

public class FragmentMachuPicchu extends Fragment {

    public static FragmentMachuPicchu newInstance()
    {
        return new FragmentMachuPicchu();
    }

    private ImageView imageViewMachuPicchu;
    private TextView textViewMachuPicchu;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_machu_picchu,container,false);

        imageViewMachuPicchu = view.findViewById(R.id.imageViewMachuPicchu);
        textViewMachuPicchu = view.findViewById(R.id.textViewMachuPicchu);

        return view;
    }
}
