package com.ongel.informationbook.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;

public class FragmentColosseum extends Fragment {

    public static FragmentColosseum newInstance()
    {
        return new FragmentColosseum();
    }

    private ImageView imageViewColosseum;
    private TextView textViewColosseum;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_colosseum,container,false);

        imageViewColosseum = view.findViewById(R.id.imageViewColosseum);
        textViewColosseum = view.findViewById(R.id.textViewColosseum);

        return view;
    }
}