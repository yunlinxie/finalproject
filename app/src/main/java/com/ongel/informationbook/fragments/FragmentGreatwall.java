package com.ongel.informationbook.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;

public class FragmentGreatwall extends Fragment {

    public static FragmentGreatwall newInstance()
    {
        return new FragmentGreatwall();
    }

    private ImageView imageViewGreatwall;
    private TextView textViewGreatwall;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_greatwall,container,false);

        imageViewGreatwall = view.findViewById(R.id.imageViewGreatwall);
        textViewGreatwall = view.findViewById(R.id.textViewGreatwall);

        return view;
    }
}
