package com.ongel.informationbook.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;


public class FragmentUS extends Fragment {

    public static FragmentUS newInstance()
    {
        return new FragmentUS();
    }

    private ImageView imageViewUS;
    private TextView textViewUS;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_united_states_of_america,container,false);

        imageViewUS = view.findViewById(R.id.imageViewUS);
        textViewUS = view.findViewById(R.id.textViewUS);

        return view;
    }
}
