package com.ongel.informationbook.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;

public class FragmentBritishMuseum extends Fragment {

    public static FragmentBritishMuseum newInstance()
    {
        return new FragmentBritishMuseum();
    }

    private ImageView imageViewBritishMuseum;
    private TextView textViewBritishMuseum;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_british_museum,container,false);

        imageViewBritishMuseum = view.findViewById(R.id.imageViewBritishMuseum);
        textViewBritishMuseum = view.findViewById(R.id.textViewBritishMuseum);

        return view;
    }
}
