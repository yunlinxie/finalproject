package com.ongel.informationbook.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.ongel.informationbook.R;

public class FragmentMetropolitan extends Fragment {

    public static FragmentMetropolitan newInstance()
    {
        return new FragmentMetropolitan();
    }

    private ImageView imageViewMetropolitan;
    private TextView textViewMetropolitan;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_metropolitan,container,false);

        imageViewMetropolitan = view.findViewById(R.id.imageViewMetropolitan);
        textViewMetropolitan = view.findViewById(R.id.textViewMetropolitan);

        return view;
    }
}
